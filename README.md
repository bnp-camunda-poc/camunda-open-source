# Camunda_SpringBoot
--------------------------------------------------------------------------

###For Writing Test Cases

Step1 : Add dependencies
    <artifactId>camunda-bpm-process-test-coverage</artifactId>
    <artifactId>camunda-bpm-assert</artifactId>
    <artifactId>assertj-core</artifactId>
    <artifactId>powermock-module-junit4</artifactId>
    <artifactId>mockito-core</artifactId>

Step2 : In your test case file register the processEnging by creating object process Enging Rule
 Ex: public ProcessEngineRule rule = new ProcessEngineRule();
 
 That's it nothing more, happy testing...

-----------------------------------------------------------------------------------