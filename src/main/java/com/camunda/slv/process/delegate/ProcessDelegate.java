/**
 * 
 */
package com.camunda.slv.process.delegate;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

/**
 * @author selvark
 *
 */

public class ProcessDelegate implements JavaDelegate {

	@Override
	public void execute(DelegateExecution execution) throws Exception {
		String question=(String) execution.getVariable("question");
		execution.getProcessEngineServices().getRuntimeService().createMessageCorrelation("askEmployee")
		.setVariable("question", question)
		.correlate();
	}

}
