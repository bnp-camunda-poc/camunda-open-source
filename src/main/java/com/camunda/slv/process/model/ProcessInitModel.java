/**
 * 
 */
package com.camunda.slv.process.model;

import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * @author selvark
 *
 */
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ProcessInitModel {
	private String messageName;
	private String businessKey;
	private String operationName;
	private String operationStatus;
	
}
