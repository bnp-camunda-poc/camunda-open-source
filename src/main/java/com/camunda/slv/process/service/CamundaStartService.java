package com.camunda.slv.process.service;

import org.camunda.bpm.engine.RuntimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.camunda.slv.process.model.ProcessInitModel;

@Service
public class CamundaStartService {
    @Autowired
    RuntimeService runtimeService;

    public void startProcessByMessage(ProcessInitModel initModel) {
        runtimeService.createMessageCorrelation(initModel.getMessageName()).processInstanceBusinessKey(initModel.getBusinessKey())
                .setVariable("businessKey", initModel.getBusinessKey())
                .setVariable(initModel.getOperationName(),initModel.getOperationStatus())
                .correlate();
    }

    public void startProcessByMessage2(ProcessInitModel msg) throws Exception {
       

               runtimeService.createMessageCorrelation(msg.getMessageName()).processInstanceBusinessKey(msg.getBusinessKey())
                .setVariable(msg.getOperationName(),msg.getOperationStatus())
                .correlate();


    }
}
