package com.camunda.slv.process.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.camunda.slv.process.model.ProcessInitModel;
import com.camunda.slv.process.service.CamundaStartService;

@RestController
public class MessageController {

    @Autowired
    CamundaStartService camundaStartService;


    @RequestMapping("/get")
    public String index() {
        return "Greetings from Spring Boot!";
    }

    @RequestMapping(value = "/engine-rest/message", method = RequestMethod.POST)
    public void persistPerson(@RequestBody ProcessInitModel obj) throws Exception {
        camundaStartService.startProcessByMessage(obj);
    }



    
}
